﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* 
 * Page Author: Jay Weber
 * Purpose: To provide a connection to the travel experts database
 * Date: June 25 2016
 * 
 */
namespace TravelExperts
{
    public static class TravelExpertsDB
    {
        public static SqlConnection GetConnection()
        {
            SqlConnection connection = null;
            string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\TravelExperts.mdf;Integrated Security=True;Connect Timeout=30";
            connection = new SqlConnection(connectionString);
            return connection;
        }
    }
}
