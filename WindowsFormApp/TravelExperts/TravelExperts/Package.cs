﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* 
 * Page Author: Jay Weber
 * Purpose: Have a business level class for the packages, with private properties
 * Date: June 25 2016
 * 
 */
namespace TravelExperts
{

    public class Package
    {

        public int PackageID { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Description { get; set; }

        public decimal BasePrice { get; set; }

        public decimal AgencyCommission { get; set; }

        public Package()
        {

        }
    }
}
