﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* 
 * Page Author: Jay Weber
 * Purpose: To have a Database level static class that has all the methods for selecting, inserting, and deleting 
 * Date: June 28 2016
 * 
 */
namespace TravelExperts
{
    public static class PackageDB
    {
        

        public static List<Package> GetPackages()
        {
            List<Package> packages = new List<Package>(); // empty list
            Package package = null; // package object
            SqlConnection conn = TravelExpertsDB.GetConnection();
            string query = "SELECT * FROM Packages " +
                           "INNER JOIN Packages_Products_Suppliers " +
                           "ON Packages.PackageId = Packages_Products_Suppliers.PackageId " +
                           "INNER JOIN Products_Suppliers " +
                           "ON Packages_Products_Suppliers.ProductSupplierId = Products_Suppliers.ProductSupplierId " +
                           "INNER JOIN Products " + 
                           "ON Products.ProductId = Products_Suppliers.ProductId";
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataReader reader; // reader object

            try
            {
                conn.Open();
                reader = command.ExecuteReader(CommandBehavior.Default);
                while (reader.Read()) // if there is a package to be read
                {
                    package = new Package();
                    package.PackageID = (int)reader["PackageID"];
                    package.Name = reader["PkgName"].ToString();
                    package.StartDate = (DateTime)reader["PkgStartDate"];
                    package.EndDate = (DateTime)reader["PkgEndDate"];
                    package.Description = reader["PkgDesc"].ToString();
                    package.BasePrice = Convert.ToDecimal(reader["PkgBasePrice"]);
                    package.AgencyCommission = Convert.ToDecimal(reader["PkgAgencyCommission"]);
                    packages.Add(package);
                }
            }
            catch (SqlException ex)
            {

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return packages;
        }

        public static bool UpdatePackage(Package oldPackage, Package newPackage)
        {
            bool result = false; // unsuccesful update
            SqlConnection conn = TravelExpertsDB.GetConnection();
            string statement = "UPDATE Packages SET " +
                           "PkgName = @NewName, " +
                           "PkgStartDate = @NewStartDate, " +
                           "PkgEndDate = @NewEndDate, " +
                           "PkgDesc = @NewDescription, " +
                           "PkgBasePrice = @NewBasePrice " +
                           "PkgAgencyCommission = @NewAgencyCommission " +
                           "WHERE PackageID = @OldPackageID" +
                           "AND PkgName = @OldName" +
                           "AND PkgStartDate = @OldStartDate" +
                           "AND PkgEndDate = @OldEndDate" +
                           "AND PkgDesc = @OldDescription" +
                           "AND PkgBasePrice = @OldBasePrice" +
                           "AND PkgAgencyCommission = @OldAgencyCommission";

            SqlCommand command = new SqlCommand(statement, conn);
            // add all parameters with values
            command.Parameters.AddWithValue("@NewName", newPackage.Name);
            command.Parameters.AddWithValue("@NewStartDate", newPackage.StartDate);
            command.Parameters.AddWithValue("@NewEndDate", newPackage.EndDate);
            command.Parameters.AddWithValue("@NewDescription", newPackage.Description);
            command.Parameters.AddWithValue("@NewBasePrice", newPackage.BasePrice);
            command.Parameters.AddWithValue("@NewAgencyCommission", newPackage.BasePrice);

            command.Parameters.AddWithValue("@OldPackageID", oldPackage.PackageID);
            command.Parameters.AddWithValue("@OldName", oldPackage.Name);
            command.Parameters.AddWithValue("@OldStartDate", oldPackage.StartDate);
            command.Parameters.AddWithValue("@OldEndDate", oldPackage.EndDate);
            command.Parameters.AddWithValue("@OldDescription", oldPackage.Description);
            command.Parameters.AddWithValue("@OldBasePrice", oldPackage.BasePrice);
            command.Parameters.AddWithValue("@OldAgencyCommission", oldPackage.AgencyCommission);


            try
            {
                conn.Open();
                int count = command.ExecuteNonQuery(); // run the update command
                if (count > 0)
                    result = true; // successful update
            }
            catch (SqlException ex)
            {

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static int AddPackage(Package package)
        {
            int PackageID = 0; // intialized
            SqlConnection conn = TravelExpertsDB.GetConnection();
            string statement = "INSERT INTO Packages (PkgName, PkgStartDate, PkgEndDate, PkgDesc, PkgBasePrice, PkgAgencyCommission) " +
                           "VALUES (@Name, @StartDate, @EndDate, @Description, @BasePrice, @AgencyCommission)";
            SqlCommand command = new SqlCommand(statement, conn);
            // parameters for command
            command.Parameters.AddWithValue("@Name", package.Name);
            command.Parameters.AddWithValue("@StartDate", package.StartDate);
            command.Parameters.AddWithValue("@EndDate", package.EndDate);
            command.Parameters.AddWithValue("@Description", package.Description);
            command.Parameters.AddWithValue("@BasePrice", package.BasePrice);
            command.Parameters.AddWithValue("@AgencyCommission", package.AgencyCommission);

            try
            {
                conn.Open();
                command.ExecuteNonQuery(); // run the command
                string query = "SELECT IDENT_CURRENT('Packages') FROM Packages";
                SqlCommand selectCommand = new SqlCommand(query, conn);
                PackageID = Convert.ToInt32(selectCommand.ExecuteScalar());

            }
            catch (SqlException ex)
            {

                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return PackageID;
        }

        public static bool DeletePackage(Package package) // old customer object
        {
            bool result = false; // not succesful

            SqlConnection conn = TravelExpertsDB.GetConnection();
            string statement = "DELETE FROM Packages " +
                                    "WHERE PackageID = @PackageID " +
                                    "AND PkgName = @Name " +
                                    "AND PkgStartDate = @StartDate " +
                                    "AND PkgEndDate = @EndDate " +
                                    "AND PkgDesc = @Description " +
                                    "AND PkgBasePrice = @BasePrice" + 
                                    "AND PkgAgencyCommission = @AgencyCommission";
            SqlCommand command = new SqlCommand(statement, conn);
            // parameters
            command.Parameters.AddWithValue("@PackageID", package.PackageID);
            command.Parameters.AddWithValue("@Name", package.Name);
            command.Parameters.AddWithValue("@StartDate", package.StartDate);
            command.Parameters.AddWithValue("@EndDate", package.EndDate);
            command.Parameters.AddWithValue("@Description", package.Description);
            command.Parameters.AddWithValue("@BasePrice", package.BasePrice);
            command.Parameters.AddWithValue("@AgencyCommission", package.AgencyCommission);

            try
            {
                conn.Open();
                int count = command.ExecuteNonQuery(); // run the delete command
                if (count > 0)
                    result = true; // succesful deletion
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return result;
        }
    }
}
